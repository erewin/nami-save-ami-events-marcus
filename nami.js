var namiLib = require("nami/src/nami.js");
//var syslog = require('modern-syslog');
const fs = require('fs');
var mysql = require('mysql');

const output = fs.createWriteStream('/var/log/errors-nami.log');
const logger = new console.Console(output);

var db_config = {
    host: "localhost",
    user: "root",
    database: "asterisk_logs",
    password: ""
};

var con = mysql.createConnection(db_config);

//access to mysql configuration
  con.connect(function(err) {
    if (err) {
        console.log(util.inspect(err));
        //syslog.err(util.inspect(err)); 
        logger.log(util.inspect(err));
    
    }
        
  });
//

//access to asterisk configuration
var namiConfig = {
    host: "127.0.0.1",
    port: "5038",
    username: "monit",
    secret: "116256"
};
//

var nami = new namiLib.Nami(namiConfig);

function handleDisconnect() {
    con = mysql.createConnection(db_config); // Recreate the connection, since

    con.connect(function(err) {              // The server is either down
      if(err) {                              // or restarting (takes a while sometimes).
        console.log('error when connecting to db:', err);
        //syslog.log('error when connecting to db:', err)
        logger.log('error when connecting to db: %s', err)
        setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
      }                                     // to avoid a hot loop, and to allow our node script to
    });
}

con.on('error', function(err) {
    console.log('db error', err);
        if(err.fatal === true) {                        // Connection to the MySQL server is usually
            handleDisconnect();                         // lost due to either server restart, or a

        } else {                                        // connnection idle timeout (the wait_timeout
            throw err;                                  // server variable configures this)
        }
  });

process.on('SIGINT', function () {
    nami.close();
    process.exit();
});

nami.on('namiConnectionClose', function (data) {
    console.log('Reconnecting...');
    setTimeout(function () { nami.open(); }, 5000);
});

nami.on('namiInvalidPeer', function (data) {
	console.log("Invalid AMI Salute. Not an AMI?");
	process.exit();
});
nami.on('namiLoginIncorrect', function () {
	console.log("Invalid Credentials");
	process.exit();
});

nami.on('namiEvent', function (event) {
    var events_name = [
        "AgentConnect",
        "QueueCallerLeave",
        "QueueCallerAbandon",
        "AgentRingNoAnswer",
        "AgentComplete",
        "QueueMemberStatus"
    ]; //array of events that will be catched by script

    if (events_name.indexOf(event.event) >= 0) { 

        new_event = {};                 //Preparing new_event: delete few properties
        
        for (key in event) {    //removing from event object useless properties 
        
            if (event.hasOwnProperty(key)) {
                if ( key !== 'privilege' && key !== 'event' && key !== 'lines' && key !== 'EOL' && key !== 'variables') {
     
                new_event[key] = event[key];
                }
            }   
        }
   
    console.log('Got Event: ' + util.inspect(event.event));

    var sql = "INSERT INTO " + event.event + " set ?";
   
     console.log(util.inspect(new_event));
     //   if (con) 
            con.query(sql, new_event, function (err, result) {
                if (err) {
                    logger.log(util.inspect(err.sqlMessage));
                    logger.log("SQLAMIERROR: " + err.sql);
                    //syslog.err(err.sqlMessage);
                    //syslog.err("SQLAMIERROR: " + err.sql);    //save sql query to syslog if something goes wrong
                    };
                
   
            if (result)
                console.log("Number of records inserted: " + result.affectedRows);
            });
          

   }




});

nami.open(); 

