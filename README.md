**AMI FREEPBX NAMI SYSLOG NODEJS**

В этом проекте нам нужно сохранять различные события в очередях Астериска через nodejs библиотеку NAMI.  
Сложности тут на первый взгляд только одна, это создания таблиц для сохранения событий. 
На самом деле нет, есть еще задача сохранения неудачного запроса, есть задача выполнения асинхронного запроса, чтобы не скрипт не тормозил. А так же восстановление связи с астериском и базой данных в случае утери

---

## Описание

В проект включены файлы:
tables.sql - который создает базу данных asterisk_logs и несколько табличек в ней, 
table-ami-events.ods - excel файл с описанием событий по столбцам. 

---

## Установка
0. устанавливаем "npm install mysql nami" версия node не ниже 8.3.1
1. создаем БД и таблички tables.sql,
2. добавляем пользователя monit в manager_custom.conf
3. прописываем учетные данные для mysql и asterisk manager в массивы db_config и namiConfig.
4. Запускаем скрипт nami.js и если модули nodejs установлены верно, то скрипт запустится даже если у вас нет астериска и базы данных.

---

## Заключение

Особенностью этого скрипта является то, что даже при пропадании подключения к mysql он будет пытаться реконнектится, 
при пропадании к астериску тоже. И если если подключение к астериску есть, а к mysql нет, то он будет сохранять готовые для импорта запросы в syslog. 

---
## Statistics that can be collected

 1. waiting time of callers till call per agent/all //
 	waiting of callers till an agent picks up (need per day and per agent/ day)//
	
 2. waiting time of callers before hangup //
 	waiting of callers in the queue (if not agent picks up in time) callers will hangup..
	so i want to calculate the average waiting (better median maybe)//
	 
 3. agent online time/day
 4. agent talking time/day
 
## Logs

	/var/log/errors-nami.log 
	      show errors when insert in mysql
	/var/log/events-nami.log
		show events that known\unknown by script, inserted or not. 
		

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).